#include "SingleExtractor.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsSingleExtractor.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

#define ONE_HG

// Constructor
SingleExtractor::SingleExtractor()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("pulsesTPCHG");
    m_event->IncludeBranch("pulsesTPCLG");
    m_event->IncludeBranch("eventHeader");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("SingleExtractor Analysis");

    // Setup the analysis specific cuts.
    m_cutsSingleExtractor = new CutsSingleExtractor(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
SingleExtractor::~SingleExtractor()
{
    delete m_cutsSingleExtractor;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void SingleExtractor::Initialize()
{
    INFO("Initializing SingleExtractor Analysis");
}

// Execute() - Called once per event.
void SingleExtractor::Execute()
{

    // Make a variable from the contents of the data
    //  prevents having to write full ' (*event->Branch)->variableName ' structure many times in the analysis
    int numSS = (*m_event->m_singleScatter)->nSingleScatters;
    int npulses = (*m_event->m_tpcPulses)->nPulses;
    ULong_t timestamp = (*m_event->m_eventHeader)->triggerTimeStamp_s;
    ULong_t runID = (*m_event->m_eventHeader)->runID;
    ULong_t eventID = (*m_event->m_eventHeader)->eventID;

    // process only single scatters 
    if (numSS != 1)
        return;
    
    auto SS = *m_event->m_singleScatter;
    auto TPC = *m_event->m_tpcPulses;
    auto TPCHG = *m_event->m_tpcHGPulses;
    auto TPCLG = *m_event->m_tpcLGPulses;
    
    int s1_id = SS->s1PulseID;
    int s2_id = SS->s2PulseID;
    
    float s2x = SS->x_cm*10.;
    float s2y = SS->y_cm*10.;
    float s2r = sqrt(s2x*s2x + s2y*s2y);

// ==== read S2 ====
    float s2_top = 0;
    float s2_bot = 0;
    int nsatt = 0;
    int nsatb = 0;
    std::vector<double> area_top (253, 0.0);
    std::vector<bool> sat_top (253, false);
    std::vector<double> area_bot (241, 0.0);
    std::vector<bool> sat_bot (241, false); 
    
    vector<int> ch_ids = TPC->chID[s2_id];
    vector<float> chPulseArea_phd = TPC->chPulseArea_phd[s2_id];
    vector<bool> chSaturated = TPC->chSaturated[s2_id];

// read channel information
    for (int i = 0; i < ch_ids.size(); i++) {
        int chi = ch_ids[i];

        if (chi >= 1000)
            chi -= 1000;

        if (chi >= 0 && chi < 253) {
            area_top[chi] = chPulseArea_phd[i];
            sat_top[chi] = chSaturated[i];
            s2_top += chPulseArea_phd[i];
            if (chSaturated[i])
                nsatt++;
        }
        chi -= 300;
        if (chi >= 0 && chi < 241) {
            area_bot[chi] = chPulseArea_phd[i];
            sat_bot[chi] = chSaturated[i];
            s2_bot += chPulseArea_phd[i];
            if (chSaturated[i])
                nsatb++;            
        }
    }
    
// ==== read S2 HG====
    std::vector<double> area_top_hg (253, 0.0);
    std::vector<bool> sat_top_hg (253, false);
    std::vector<double> area_bot_hg (241, 0.0);
    std::vector<bool> sat_bot_hg (241, false); 
    
    vector<int> ch_ids_hg = TPCHG->chID[s2_id];
    vector<float> chPulseArea_phd_hg = TPCHG->chPulseArea_phd[s2_id];
    vector<bool> chSaturated_hg = TPCHG->chSaturated[s2_id];
   
// read channel information
    std::cout << "size: "<< ch_ids_hg.size() << std::endl;
    for (int i = 0; i < ch_ids_hg.size(); i++) {
        int chi = ch_ids_hg[i];

        if (chi >= 0 && chi < 253) {
            area_top_hg[chi] = chPulseArea_phd_hg[i];
            sat_top_hg[chi] = chSaturated_hg[i];
        }
        chi -= 300;
        if (chi >= 0 && chi < 241) {
            area_bot_hg[chi] = chPulseArea_phd_hg[i];
            sat_bot_hg[chi] = chSaturated_hg[i];           
        }
    }

// ==== read S2 LG====
    // Match LG s2 to HG s2
#ifdef ONE_HG    
// For this find the largst LG pulse that fits (within tolerance) inside HG s2 pulse
    int tol = 2500; // tolerance 
    int hg_start = (TPCHG->pulseStartTime_ns)[s2_id] - tol;
    int hg_end = (TPCHG->pulseEndTime_ns)[s2_id] + tol;
    vector <int> lg_start = TPCLG->pulseStartTime_ns;
    vector <int> lg_end = TPCHG->pulseEndTime_ns;
    vector <float> lg_area_phd = TPCLG->pulseArea_phd;

    int lg_size = lg_start.size();
    int lg_s2_id = -1;
    for (int i=0; i<lg_size; i++) {
//            std::cout << "s2 LG: " << lg_start[i] << " - " << lg_end[i] << ", " << lg_area_phd[i] << std::endl;
        if ( lg_start[i]>hg_start && lg_end[i]<hg_end ) {
            if (lg_s2_id < 0)
                lg_s2_id = i;
            else
                if (lg_area_phd[i] > lg_area_phd[lg_s2_id])
                    lg_s2_id = i;
        }
    }
    
    if (lg_s2_id == -1) 
        return;
    
    std::vector<double> area_top_lg (253, 0.0);
    std::vector<bool> sat_top_lg (253, false);
    std::vector<double> area_bot_lg (241, 0.0);
    std::vector<bool> sat_bot_lg (241, false); 
    
    vector<int> ch_ids_lg = TPCLG->chID[lg_s2_id];
    vector<float> chPulseArea_phd_lg = TPCLG->chPulseArea_phd[lg_s2_id];
    vector<bool> chSaturated_lg = TPCLG->chSaturated[lg_s2_id];

// read channel information
    std::cout << "size: "<< ch_ids_lg.size() << std::endl;
    for (int i = 0; i < ch_ids_lg.size(); i++) {
        int chi = ch_ids_lg[i] - 1000;

        if (chi >= 0 && chi < 253) {
            area_top_lg[chi] = chPulseArea_phd_lg[i];
            sat_top_lg[chi] = chSaturated_lg[i];
        }
        chi -= 300;
        if (chi >= 0 && chi < 241) {
            area_bot_lg[chi] = chPulseArea_phd_lg[i];
            sat_bot_lg[chi] = chSaturated_lg[i];           
        }
    }
    
#else
// For this find all the LG pulses that fit (within tolerance) inside HG s2 pulse
    int tol = 2500; // tolerance 
    int hg_start = (TPCHG->pulseStartTime_ns)[s2_id] - tol;
    int hg_end = (TPCHG->pulseEndTime_ns)[s2_id] + tol;
    vector <int> lg_start = TPCLG->pulseStartTime_ns;
    vector <int> lg_end = TPCHG->pulseEndTime_ns;
    vector <float> lg_area_phd = TPCLG->pulseArea_phd;

    int lg_size = lg_start.size();
    std::vector <int> vec_lgid2;
    for (int i=0; i<lg_size; i++) {

//            std::cout << "s2 LG: " << lg_start[i] << " - " << lg_end[i] << ", " << lg_area_phd[i] << std::endl;
        if ( lg_start[i]>hg_start && lg_end[i]<hg_end ) {
            vec_lgid2.push_back(i);
        }
    }
    
    int lgid2_len = vec_lgid2.size();
    if (lgid2_len == 0) 
        return;
    
    std::vector<double> area_top_lg (253, 0.0);
    std::vector<bool> sat_top_lg (253, false);
    std::vector<double> area_bot_lg (241, 0.0);
    std::vector<bool> sat_bot_lg (241, false); 
    
    for (int lg_s2_id : vec_lgid2) {
        vector<int> ch_ids_lg = TPCLG->chID[lg_s2_id];
        vector<float> chPulseArea_phd_lg = TPCLG->chPulseArea_phd[lg_s2_id];
        vector<bool> chSaturated_lg = TPCLG->chSaturated[lg_s2_id];

    // read channel information
//        std::cout << "size: "<< ch_ids_lg.size() << std::endl;
        for (int i = 0; i < ch_ids_lg.size(); i++) {
            int chi = ch_ids_lg[i] - 1000;

            if (chi >= 0 && chi < 253) {
                area_top_lg[chi] += chPulseArea_phd_lg[i];
                sat_top_lg[chi] = sat_top_lg[chi] || chSaturated_lg[i];
            }
            chi -= 300;
            if (chi >= 0 && chi < 241) {
                area_bot_lg[chi] += chPulseArea_phd_lg[i];
                sat_bot_lg[chi] = sat_bot_lg[chi] || chSaturated_lg[i];           
            }
        }
    }
#endif

    
// ==== read S1 ====
    float s1_top = 0;
    float s1_bot = 0;
    int s1_nsatt = 0;
    int s1_nsatb = 0;
    std::vector<double> s1_area_top (253, 0.0);
    std::vector<bool> s1_sat_top (253, false);
    std::vector<double> s1_area_bot (241, 0.0);
    std::vector<bool> s1_sat_bot (241, false);
 
    vector<int> s1_ch_ids = TPC->chID[s1_id];
    vector<float> s1_chPulseArea_phd = TPC->chPulseArea_phd[s1_id];
    vector<bool> s1_chSaturated = TPC->chSaturated[s1_id];

// read channel information
    for (int i = 0; i < s1_ch_ids.size(); i++) {
        int chi = s1_ch_ids[i];

        if (chi >= 1000)
            chi -= 1000;

        if (chi >= 0 && chi < 253) {
            s1_area_top[chi] = s1_chPulseArea_phd[i];
            s1_sat_top[chi] = s1_chSaturated[i];
            s1_top += s1_chPulseArea_phd[i];
            if (s1_chSaturated[i])
                s1_nsatt++;
        }
        chi -= 300;
        if (chi >= 0 && chi < 241) {
            s1_area_bot[chi] = s1_chPulseArea_phd[i];
            s1_sat_bot[chi] = s1_chSaturated[i];
            s1_bot += s1_chPulseArea_phd[i];
            if (s1_chSaturated[i])
                s1_nsatb++;            
        }
    }
    
    m_hists->BookFillHist("SS_S1", 300, 0., 3000., SS->s1Area_phd);
    m_hists->BookFillHist("SS_S2", 300, 0., 1000000., SS->s2Area_phd);
    
    std::string var_names("s1area:s2area:s2x:s2y:s2r:s2top:s2bot:nsatt:nsatb:s1top:s1bot:s1nsatt:s1nsatb:dtime:ts:runid:evtid:pmt[1482]");
//        std::string var_names("s2x:s2y:v[10]");
    double vars[2048];

    vars[0] = SS->s1Area_phd;
    vars[1] = SS->s2Area_phd;
    vars[2] = s2x;
    vars[3] = s2y;
    vars[4] = s2r;
    vars[5] = s2_top;
    vars[6] = s2_bot;
    vars[7] = nsatt;
    vars[8] = nsatb;
    vars[9] = s1_top;
    vars[10] = s1_bot;
    vars[11] = s1_nsatt;
    vars[12] = s1_nsatb;
    vars[13] = SS->driftTime_ns*0.001;
    vars[14] = timestamp - 1.63358e9;
    vars[15] = runID;
    vars[16] = eventID;
        
    int base = 17;
    
    for (size_t i=0; i<253; i++)
        vars[base+i] = sat_top_hg[i] ? -area_top_hg[i] : area_top_hg[i];
    for (size_t i=0; i<241; i++)
        vars[base+253+i] = sat_bot_hg[i] ? -area_bot_hg[i] : area_bot_hg[i];

    for (size_t i=0; i<253; i++)
//        vars[base+253+i] = sat_top_lg[i] ? -area_top_lg[i] : area_top_lg[i];
        vars[base+494+i] = sat_top_lg[i] ? -area_top_lg[i] : area_top_lg[i];
    for (size_t i=0; i<241; i++)
        vars[base+747+i] = sat_bot_lg[i] ? -area_bot_lg[i] : area_bot_lg[i];
    
/*    
    for (size_t i=0; i<253; i++)
//        vars[base+506+i] = sat_top[i] ? -area_top[i] : area_top[i];
        vars[base+i] = sat_top[i] ? -area_top[i] : area_top[i];
    for (size_t i=0; i<241; i++)
        vars[base+253+i] = sat_bot[i] ? -area_bot[i] : area_bot[i];
*/         
    for (size_t i=0; i<253; i++)
        vars[base+988+i] = s1_sat_top[i] ? -s1_area_top[i] : s1_area_top[i];
    for (size_t i=0; i<241; i++)
        vars[base+1241+i] = s1_sat_bot[i] ? -s1_area_bot[i] : s1_area_bot[i];
    

    m_hists->BookFillTree("SS_Tree", var_names, vars);
}

// Finalize() - Called once after event loop.
void SingleExtractor::Finalize()
{
    INFO("Finalizing SingleExtractor Analysis");
}
