#ifndef SingleExtractor_H
#define SingleExtractor_H

#include "Analysis.h"

#include "CutsSingleExtractor.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

enum Detector 
{
    HG,
    LG,
    MIX
};

class HitPattern
{
public:
    HitPattern(EventBase* my_event, Detector det, int pulse_id);
    void Add(HitPattern s);
    double Atop(int i) {return sat_top[i] ? -area_top[i] : area_top[i];}
    double Abot(int i) {return sat_bot[i] ? -area_bot[i] : area_bot[i];}
    std::vector<double> area_top; 
    std::vector<bool> sat_top; 
    std::vector<double> area_bot; 
    std::vector<bool> sat_bot; 
// sum of *non-satuarted* channels    
    double sum_top = 0; 
    double sum_bot = 0; 
// saturated channel count    
    int nsat_top = 0;
    int nsat_bot = 0;
private:
    vector<int> ch_ids;
    vector<float> chPulseArea_phd;
    vector<bool> chSaturated;
};

class SingleExtractor : public Analysis {

public:
    SingleExtractor();
    ~SingleExtractor();

    void Initialize();
    void Execute();
    void Finalize();
    int MatchLG(int pulse_id, int tol=5000);
    std::vector <int> MatchAllLG(int pulse_id, int tol=5000);

protected:
    CutsSingleExtractor* m_cutsSingleExtractor;
    ConfigSvc* m_conf;
};



#endif
