#ifndef CutsSingleExtractor_H
#define CutsSingleExtractor_H

#include "EventBase.h"

class CutsSingleExtractor {

public:
    CutsSingleExtractor(EventBase* eventBase);
    ~CutsSingleExtractor();
    bool SingleExtractorCutsOK();

private:
    EventBase* m_event;
};

#endif
