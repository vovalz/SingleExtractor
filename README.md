# SingleExtractor

This is an ALPACA module that extracts selected RQs (including TPC PMT channel information) for all single scatters passing the cuts into a ROOT tree.

## Installation

In order to correctly export arrays into ROOT tree, ALPACA needs a small patch described below. Replace the line 492 in `modules/AlpacaCore/src/HistSvc.cxx` (look for the function `HistSvc::BookTree()`)
```c++
        while (pch) {
            tree->Branch(pch, address + i++); // <---------- this line
            pch = strtok(0, ":");
        }
```
with these four lines:
```c++
        while (pch) {
            const char *brk = strchr(pch, '['); //  <-----
            int len = brk ? atoi(brk+1) : 1;    //  <-----
            tree->Branch(pch, address + i);     //  <-----
            i += len;                           //  <-----
            pch = strtok(0, ":");
        }
```
After this, follow the standard procedure for an ALPACA module installation.

## ROOT Tree structure

Tree branches are collection of RQs I find useful in analysis followed by channel data for s1 and s2 pulses. Some of these RQs come directly from LZap (with possible scaling) and some are calculated by the Extractor. The channel data is organized in six arrays listed in the end. These arrays are double precision, with ADC saturation in the channel indicated by the sign flip (negative means saturation with the absolute value equal to the original area). For s2, both HG and LG channel data is exported, making it possible to use own gain mixing for position reconstruction end energy calculation. For s1, LZap mixed data is used to save space. 

* **s1area**: total s1 area as reported by LZap (phd)
* **s2area**: total s2 area as reported by LZap (phd)
* **s2x**: s2 X position as reported by LZap (mm)
* **s2y**: s2 Y position as reported by LZap (mm)
* **s2r**: s2 distance from the TPC axis, calculated from LZap position (mm)
* **s2top**: top s2 area, sum of all not saturated top PMTs from channel data (phd)
* **s2bot**: bottom s2 area, sum of all not saturated bottom PMTs from channel data (phd)
* **s2nsatt**: number of LG channels in the top array with ADC saturated by s2 
* **s2nsatb**: number of LG channels in the bottom array with ADC saturated by s2
* **time10**: time at 10% s2 area (us)
* **time90**: time at 90% s2area (us)
* **fwhm**: FWHM duartion of s2 pulse (us)
* **s1top**: top s1 area, sum of all not saturated top PMTs from channel data (phd)
* **s1bot**: bottom s1 area, sum of all not saturated bottom PMTs from channel data (phd)
* **s1nsatt**: number of LG channels in the top array with ADC saturated by s1
* **s1nsatb**: number of LG channels in the bottom array with ADC saturated by s2
* **skinarea**: total area in Skin PMTs within skin veto window (phd) (skinTotalArea in ss branch)
* **s1id**: s1 pulse ID in single scatter
* **s2id**: s2 HG pulse ID in single scatter
* **s2lglzap**: s2 LG pulse ID in single scatter as reported by LZap (-1 if no match found)
* **s2lgmy**: s2 LG pulse ID in single scatter found as the pulse with the largest area in +/- 5 us window around HG s2 pulse
* **dtime**: single scatter drift time (delay between s1 and s2) (us)
* **ts**: trigger timestamp (s) with 1.63358e9 subtracted
* **runid**: run ID
* **evtid**: event ID
* **tops2h[253]**: s2 top HG
* **bots2h[241]**: s2 bottom HG
* **tops2l[253]**: s2 top LG
* **bots2l[241]**: s2 bottom LG
* **tops1[253]**: s1 top MIX gain
* **bots1[241]**: s1 bottom MIX gain
