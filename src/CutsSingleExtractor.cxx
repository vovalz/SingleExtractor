#include "CutsSingleExtractor.h"
#include "ConfigSvc.h"

CutsSingleExtractor::CutsSingleExtractor(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsSingleExtractor::~CutsSingleExtractor()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsSingleExtractor::SingleExtractorCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
