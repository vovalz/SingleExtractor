#include "SingleExtractor.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsSingleExtractor.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// #define MATCH_OWN
// #define MATCH_OWN_MULTI

HitPattern::HitPattern(EventBase* my_event, Detector det, int pulse_id) 
{
    area_top = std::vector<double> (253, 0.0);
    sat_top = std::vector<bool> (253, false);
    area_bot = std::vector<double> (241, 0.0);
    sat_bot = std::vector<bool> (241, false);

    if (det == MIX) {
        auto TPC = *my_event->m_tpcPulses;
        ch_ids = TPC->chID[pulse_id];
        chPulseArea_phd = TPC->chPulseArea_phd[pulse_id];
        chSaturated = TPC->chSaturated[pulse_id];
    } else if (det == HG) {
        auto TPCHG = *my_event->m_tpcHGPulses;
        ch_ids = TPCHG->chID[pulse_id];
        chPulseArea_phd = TPCHG->chPulseArea_phd[pulse_id];
        chSaturated = TPCHG->chSaturated[pulse_id];            
    } else if (det == LG) {
        auto TPCLG = *my_event->m_tpcLGPulses;
        ch_ids = TPCLG->chID[pulse_id];
        chPulseArea_phd = TPCLG->chPulseArea_phd[pulse_id];
        chSaturated = TPCLG->chSaturated[pulse_id];            
    }

// read channel information
    for (int i = 0; i < ch_ids.size(); i++) {
        int chi = ch_ids[i];
        if (chi >= 1000)
            chi -= 1000;

        if (chi >= 0 && chi < 253) {
            area_top[chi] = chPulseArea_phd[i];
            sat_top[chi] = chSaturated[i];
            if (chSaturated[i])
                nsat_top++;
            else
                sum_top += chPulseArea_phd[i];
        }
        chi -= 300;
        if (chi >= 0 && chi < 241) {
            area_bot[chi] = chPulseArea_phd[i];
            sat_bot[chi] = chSaturated[i];
            if (chSaturated[i])
                nsat_bot++;
            else
                sum_bot += chPulseArea_phd[i];          
        }
    }
}

void HitPattern::Add(HitPattern s) 
{
    sum_top = sum_bot = nsat_top = nsat_bot = 0;
    for (int i=0; i<253; i++) {
        area_top[i] += s.area_top[i];
        sat_top[i] = sat_top[i] | s.sat_top[i];
        if (sat_top[i])
            nsat_top++;
        else
            sum_top += area_top[i];            
    }
    for (int i=0; i<241; i++) {
        area_bot[i] += s.area_bot[i];
        sat_bot[i] = sat_bot[i] | s.sat_bot[i];
        if (sat_bot[i])
            nsat_bot++;
        else
            sum_bot += area_bot[i];            
    }
}  

// Constructor
SingleExtractor::SingleExtractor()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("pulsesTPCHG");
    m_event->IncludeBranch("pulsesTPCLG");
    m_event->IncludeBranch("eventHeader");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("SingleExtractor Analysis");

    // Setup the analysis specific cuts.
    m_cutsSingleExtractor = new CutsSingleExtractor(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
SingleExtractor::~SingleExtractor()
{
    delete m_cutsSingleExtractor;
}

// --------------- Helper functions ----------------

// find the largest LG pulse matching pulse pulse_id in HG within +-tol ns from the HG pulse start
// returns the id of the largest LG pulse or -1 if there is no match
int SingleExtractor::MatchLG(int pulse_id, int tol)
{
    auto TPCHG = *m_event->m_tpcHGPulses;
    auto TPCLG = *m_event->m_tpcLGPulses;    
    int hg_start = (TPCHG->pulseStartTime_ns)[pulse_id] - tol;
    int hg_end = (TPCHG->pulseEndTime_ns)[pulse_id] + tol;
    vector <int> lg_start = TPCLG->pulseStartTime_ns;
    vector <int> lg_end = TPCHG->pulseEndTime_ns;
    vector <float> lg_area_phd = TPCLG->pulseArea_phd;

    int lg_size = lg_start.size();
    int lg_id = -1;
    for (int i=0; i<lg_size; i++) {
        if ( lg_start[i]>hg_start && lg_end[i]<hg_end ) {
            if (lg_id < 0)
                lg_id = i;
            else
                if (lg_area_phd[i] > lg_area_phd[lg_id])
                    lg_id = i;
        }
    }
    return lg_id;
}

// find all LG pulses matching pulse pulse_id in HG within +-tol ns from the HG pulse start
// returns vector of ids of found LG pulses or an empty vector if there is no match
std::vector <int> SingleExtractor::MatchAllLG(int pulse_id, int tol)
{
    auto TPCHG = *m_event->m_tpcHGPulses;
    auto TPCLG = *m_event->m_tpcLGPulses;
    int hg_start = (TPCHG->pulseStartTime_ns)[pulse_id] - tol;
    int hg_end = (TPCHG->pulseEndTime_ns)[pulse_id] + tol;
    vector <int> lg_start = TPCLG->pulseStartTime_ns;
    vector <int> lg_end = TPCHG->pulseEndTime_ns;
    vector <float> lg_area_phd = TPCLG->pulseArea_phd;

    int lg_size = lg_start.size();
    std::vector <int> vec_lgid2;
    for (int i=0; i<lg_size; i++) {
        if ( lg_start[i]>hg_start && lg_end[i]<hg_end ) {
            vec_lgid2.push_back(i);
        }
    }
    return vec_lgid2;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void SingleExtractor::Initialize()
{
    INFO("Initializing SingleExtractor Analysis");
}

// Execute() - Called once per event.
void SingleExtractor::Execute()
{

    // Make a variable from the contents of the data
    //  prevents having to write full ' (*event->Branch)->variableName ' structure many times in the analysis
    int numSS = (*m_event->m_singleScatter)->nSingleScatters;
    int npulses = (*m_event->m_tpcPulses)->nPulses;
    ULong_t timestamp = (*m_event->m_eventHeader)->triggerTimeStamp_s;
    ULong_t runID = (*m_event->m_eventHeader)->runID;
    ULong_t eventID = (*m_event->m_eventHeader)->eventID;

    // process only single scatters 
    if (numSS != 1)
        return;
    
    auto SS = *m_event->m_singleScatter;
    auto TPC = *m_event->m_tpcPulses;
    auto TPCHG = *m_event->m_tpcHGPulses;
    auto TPCLG = *m_event->m_tpcLGPulses;
    
    int s1_id = SS->isDoubleS1 == 0 ? SS->s1PulseID : SS->s1aPulseID;
    int s1b_id = SS->isDoubleS1 == 0 ? -1 : SS->s1bPulseID; 
    int s2_id = SS->s2PulseID;
    int s2_id_lg_lzap = TPCHG->HGLGpulseID[s2_id];
    
    float s2x = SS->x_cm*10.;
    float s2y = SS->y_cm*10.;
    float s2r = sqrt(s2x*s2x + s2y*s2y);

// ==== read S2 ====
    HitPattern S2_mix(m_event, MIX, s2_id);
    HitPattern S2_hg(m_event, HG, s2_id);

#ifdef MATCH_OWN
    #ifndef MATCH_OWN_MULTI 
    // use own matching
        int s2_id_lg = MatchLG(s2_id, 5000);
        HitPattern S2_lg = s2_id_lg >= 0 ? HitPattern(m_event, LG, s2_id_lg): HitPattern(m_event, HG, s2_id);
    #else   
    // use own multi-matching
        std::vector <int> s2_lg_vec = MatchAllLG(s2_id, 5000);
        HitPattern S2_lg = s2_id_vec.size() > 0 ? HitPattern(LG, s2_id_vec[0]): HitPattern(HG, s2_id);
        for (size_t i=0; i<s2_id_vec.size(); i++)
            S2_lg.Add(HitPattern(LG, s2_id_vec[i]));
    #endif
#else    
// Use LZap matching    
    HitPattern S2_lg = s2_id_lg_lzap >= 0 ? HitPattern(m_event, LG, s2_id_lg_lzap): HitPattern(m_event, HG, s2_id);
#endif
    
// ==== read S1 ====
    HitPattern S1_mix(m_event, MIX, s1_id);
    if (s1b_id >= 0)
        S1_mix.Add(HitPattern(m_event, MIX, s1b_id));
    
//    m_hists->BookFillHist("SS_S1", 300, 0., 3000., SS->s1Area_phd);
//    m_hists->BookFillHist("SS_S2", 300, 0., 1000000., SS->s2Area_phd);
    
    std::string var_names("s1area:s2area:s2x:s2y:s2r:s2top:s2bot:s2nsatt:s2nsatb:time10:time90:fwhm:s1top:s1bot:s1nsatt:s1nsatb:skinarea:s1id:s2id:s2lglzap:s2lgmy:dtime:ts:runid:evtid:tops2h[253]:bots2h[241]:tops2l[253]:bots2l[241]:tops1[253]:bots1[241]");

    double vars[2048];

    vars[0] = SS->s1Area_phd;
    vars[1] = SS->s2Area_phd;
    vars[2] = s2x;
    vars[3] = s2y;
    vars[4] = s2r;
    vars[5] = S2_hg.sum_top;
    vars[6] = S2_hg.sum_bot;
    vars[7] = S2_hg.nsat_top;
    vars[8] = S2_hg.nsat_bot;
    vars[9] = TPC->areaFractionTime10_ns[s2_id]*0.001;
    vars[10] = TPC->areaFractionTime90_ns[s2_id]*0.001;
    vars[11] = TPC->fwhm_ns[s2_id]*0.001;
    vars[12] = S1_mix.sum_top;
    vars[13] = S1_mix.sum_bot;
    vars[14] = S1_mix.nsat_top;
    vars[15] = S1_mix.nsat_bot;
    vars[16] = SS->skinTotalArea;
    vars[17] = s1_id;
    vars[18] = s2_id;
    vars[19] = s2_id_lg_lzap;
    vars[20] = MatchLG(s2_id, 5000);        
    vars[21] = SS->driftTime_ns*0.001;
    vars[22] = timestamp - 1.63358e9;
    vars[23] = runID;
    vars[24] = eventID;
        
    int base = 25;
    
    for (size_t i=0; i<253; i++)
        vars[base+i] = S2_hg.Atop(i);
    for (size_t i=0; i<241; i++)
        vars[base+253+i] = S2_hg.Abot(i); 

    for (size_t i=0; i<253; i++)
        vars[base+494+i] = S2_lg.Atop(i);
    for (size_t i=0; i<241; i++)
        vars[base+747+i] = S2_lg.Abot(i); 
             
    for (size_t i=0; i<253; i++)
        vars[base+988+i] = S1_mix.Atop(i); 
    for (size_t i=0; i<241; i++)
        vars[base+1241+i] = S1_mix.Abot(i); 
    
    m_hists->BookFillTree("SS_Tree", var_names, vars);
}

// Finalize() - Called once after event loop.
void SingleExtractor::Finalize()
{
    INFO("Finalizing SingleExtractor Analysis");
}
