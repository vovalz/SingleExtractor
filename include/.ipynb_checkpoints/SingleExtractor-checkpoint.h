#ifndef SingleExtractor_H
#define SingleExtractor_H

#include "Analysis.h"

#include "CutsSingleExtractor.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class SingleExtractor : public Analysis {

public:
    SingleExtractor();
    ~SingleExtractor();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsSingleExtractor* m_cutsSingleExtractor;
    ConfigSvc* m_conf;
};

#endif
